﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceShip;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float stopDistance;
        [SerializeField] private float retreatDistance;

        private PlayerSpaceship spawnedPlayerShip;
        private Rigidbody2D rb;

        private void Start()
        {
            rb = enemySpaceship.GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            MoveToPlayer();

            Vector3 direction = spawnedPlayerShip.transform.position - transform.position;
            
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90f;
            rb.rotation = angle;
            
            enemySpaceship.Fire();
        }

        private void MoveToPlayer()
        {
            spawnedPlayerShip = GameManagers.Instance.spawnedPlayerShip;

            if (Vector2.Distance(transform.position, spawnedPlayerShip.transform.position) > stopDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, spawnedPlayerShip.transform.position,
                    enemySpaceship.Speed * Time.deltaTime);
                

            }
            else if (Vector2.Distance(transform.position, spawnedPlayerShip.transform.position) > stopDistance && Vector2.Distance
                (transform.position, spawnedPlayerShip.transform.position) > retreatDistance)
            {
                transform.position = this.transform.position;
                
            }
            else if (Vector2.Distance(transform.position, spawnedPlayerShip.transform.position) < retreatDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, spawnedPlayerShip.transform.position,
                    -enemySpaceship.Speed * Time.deltaTime);
            }

            /*
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
            Debug.Log(distanceToPlayer);
            
            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
            */

        }
        
    }    
}

