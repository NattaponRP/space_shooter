﻿using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform startDialog;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button returnMenuButton;
        
        public static UIManager Instance { get; private set; }
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(startDialog != null, "startDialog cannot be null");        
            Debug.Assert(scoreText != null, "scoreText cannot null");
            Debug.Assert(finalScoreText != null, "finalScoreText cannot null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(endDialog != null, "endDialog cannot be null");   
        
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
            nextLevelButton.onClick.AddListener(OnNextLevelButtonClicked);
            returnMenuButton.onClick.AddListener(OnReturnMenuButtonClicked);
            
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            GameManagers.Instance.OnRestarted += RestartUI;
            GameManagers.Instance.OnNextLevel += NextLevelUI;
            ScoreManager.Instance.OnScoreUpdated += UpdateScoreUI;
            ShowEndDialog(false);
            ShowScore(false);
            UpdateScoreUI();
        }

        private void OnStartButtonClicked()
        {
            ShowStartDialog(false);
            ShowScore(true);
            GameManagers.Instance.StartGame(0);
        }
    
        private void OnRestartButtonClicked()
        {
            SceneManager.LoadScene(0);
            ShowEndDialog(false);
            UpdateScoreUI();
            ShowScore(true);
            ShowRestart(false);
            GameManagers.Instance.StartGame(0);
            
        }

        private void OnReturnMenuButtonClicked()
        {
            SceneManager.LoadScene(0);
            ShowEndDialog(false);
            ShowScore(false);
            UpdateScoreUI();
            ShowStartDialog(true);
            ScoreManager.Instance.ResetScore();
        }

        private void OnNextLevelButtonClicked()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            UpdateScoreUI();
            ShowEndDialog(false);
            ShowNext(false);
            ShowScore(true);
            GameManagers.Instance.StartGame(SceneManager.GetActiveScene().buildIndex + 1);
        }
    
        private void UpdateScoreUI()
        {
            scoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
            finalScoreText.text = $"Player Score : {ScoreManager.Instance.GetScore()}";
        }

        private void RestartUI()
        {
            ShowScore(false);
            ShowRestart(true);
            ShowEndDialog(true);
        }

        private void NextLevelUI()
        {
            if (GameManagers.Instance.isEndgame)
            {
                ShowReturnMenu(true);
                ShowScore(false);
                ShowNext(false);
            }
            ShowScore(false);
            ShowEndDialog(true);
            ShowNext(true);
        }
    
        public void ShowScore(bool showScore)
        {
            scoreText.gameObject.SetActive(showScore);
        }

        public void ShowRestart(bool showRestart)
        {
            restartButton.gameObject.SetActive(showRestart);
        }

        public void ShowNext(bool showNext)
        {
            nextLevelButton.gameObject.SetActive(showNext);
        }

        private void ShowStartDialog(bool showDialog)
        {
            startDialog.gameObject.SetActive(showDialog);
        }

        private void ShowEndDialog(bool showDialog)
        {
            endDialog.gameObject.SetActive(showDialog);
        }

        private void ShowReturnMenu(bool returnMenu)
        {
            returnMenuButton.gameObject.SetActive(returnMenu);
        }

        private void OnDestroy()
        {
            GameManagers.Instance.OnRestarted -= RestartUI;
            ScoreManager.Instance.OnScoreUpdated -= UpdateScoreUI;            
        }
    }
}
