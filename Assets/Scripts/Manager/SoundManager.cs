﻿using System;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;
        
        public static SoundManager Instance { get; private set; }

        public enum Sound
        {
            BGM,
            PlayerFire,
            EnemyFire,
            Explosion,
            HitSound,
            BossBGM
        }
        [Serializable]
        public class SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0,1)] public float soundVolume;
            public bool loop = false;
            [HideInInspector]
            public AudioSource audioSource;
        }

        public void Awake()
        {
            Debug.Assert(soundClips != null && soundClips.Length != 0,"sound clips need to be setup");
            Debug.Assert(audioSource != null,"audioSource can't be null");

            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void Play(Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            if (soundClip.audioSource == null)
            {   
                soundClip.audioSource = gameObject.AddComponent<AudioSource>();
            }
            
            soundClip.audioSource.clip = soundClip.audioClip;
            soundClip.audioSource.volume = soundClip.soundVolume;
            soundClip.audioSource.loop = soundClip.loop;
            soundClip.audioSource.Play();
            
        }
        
        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(Sound.BGM);
        }

        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }
            }
            return null;
        }


    }
}
