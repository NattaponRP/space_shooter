﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using SpaceShip;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;


public class GameManagers : MonoBehaviour
{
    
    [SerializeField] private PlayerSpaceship playerSpaceship;
    [SerializeField] private EnemySpaceship[] enemySpaceships;
    [SerializeField] private int[] enemySpaceshipHp;
    [SerializeField] private float[] enemySpaceshipMoveSpeed;
    [SerializeField] private ScoreManager scoreManager;
    public event Action OnRestarted;
    public event Action OnNextLevel;
    [SerializeField] private int playerSpaceshipHp;
    [SerializeField] private float playerSpaceshipMoveSpeed;
    [SerializeField] private TextMeshProUGUI endGameText;

    public PlayerSpaceship spawnedPlayerShip;
    public EnemySpaceship spawnedEnemyShip;
    private int maxEnemy;
    private int enemyTotal;
    private float randomX , randomY;
    private Vector2 enemySpawnPoint;
    public bool isEndgame = false;
    private bool isPlayerDead = false;
    

    public static GameManagers Instance { get; private set; }

    private void Awake()
    {
        Debug.Assert(playerSpaceship != null, "playerSpaceship can't be null");
        Debug.Assert(scoreManager != null, "scoreManager can't be null");
        Debug.Assert(playerSpaceshipHp > 0, "playerSpaceshipHp has to be more than 0");
        Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than 0");

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
        SoundManager.Instance.PlayBGM();

    }

    public void StartGame(int sceneNumber)
    {

        switch (sceneNumber)
        {
            case 0 :
                maxEnemy = 5;
                enemyTotal = maxEnemy;
                SpawnPlayerSpaceship();
                StartCoroutine(EnemySpawnRate(0 , 1.5f));
                break;
            case 1 :
                maxEnemy = 2;
                enemyTotal = maxEnemy;
                SpawnPlayerSpaceship();
                StartCoroutine(EnemySpawnRate(1 , 3f));
                break;
            case 2 :
                maxEnemy = 1;
                enemyTotal = maxEnemy;
                isEndgame = true;
                SpawnPlayerSpaceship();
                StartCoroutine(EnemySpawnRate(2 , 5f));
                break;
            default:
                break;
        }
    }

    private void SpawnPlayerSpaceship()
    {
        spawnedPlayerShip = Instantiate(playerSpaceship);
        spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
        DontDestroyOnLoad(spawnedPlayerShip);
        isPlayerDead = false;
        spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
    }

    private void OnPlayerSpaceshipExploded()
    {
        endGameText.text = "Defeat";
        isPlayerDead = true;
        GameOver();
    }

    private void SpawnEnemySpaceship(int enemyNumber)
    {
        if (!isPlayerDead)
        {
            randomX = Random.Range(8f, -8f);
            enemySpawnPoint = new Vector2(randomX, 8f);
            spawnedEnemyShip = Instantiate(enemySpaceships[enemyNumber] , enemySpawnPoint , Quaternion.identity);
            spawnedEnemyShip.Init(enemySpaceshipHp[enemyNumber] , enemySpaceshipMoveSpeed[enemyNumber]);
            DontDestroyOnLoad(spawnedEnemyShip);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
        }
    }
    IEnumerator EnemySpawnRate(int enemyNumber , float spawnRate)
    {
        for (int i = 0; i < maxEnemy; i++)
        {
            yield return new WaitForSeconds(spawnRate);
            SpawnEnemySpaceship(enemyNumber);
        }
    }
    private void OnEnemySpaceshipExploded()
    {
        scoreManager.UpdateScore(1);
        enemyTotal -= 1;
        if (enemyTotal == 0)
        {
            endGameText.text = "Victory";
            Victory();
        }
    }
    
    private void GameOver()
    {
        DestroyRemainingShips();
        OnRestarted?.Invoke();
    }
    private void Victory()
    {
        DestroyRemainingShips();
        OnNextLevel?.Invoke();
    }
    private void DestroyRemainingShips()
    {
        var remainingEnemies= GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in remainingEnemies)
        {
            enemy.gameObject.SetActive(false);
        }
        
        var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
        foreach (var player in remainingPlayer)
        {
            player.gameObject.SetActive(false);
        }

    }
    
}
