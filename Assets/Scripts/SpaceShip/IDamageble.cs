﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceShip
{
    public interface IDamageable
    {
        event Action OnExploded;
        void TakeHit(int damage);
        void Explode();
    }
}

