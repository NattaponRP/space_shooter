﻿using System;
using System.Collections;
using System.Collections.Generic;
using EnemyShip;
using Manager;
using SpaceShip;
using UnityEngine;

namespace SpaceShip
{
    public class EnemySpaceship : BaseSpaceship, IDamageable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;

        private PlayerSpaceship spawnedPlayerShip;
        private EnemyController enemyController;
        private float fireCounter = 0;
        private Vector2 target;

        private void Update()
        {
            spawnedPlayerShip = GameManagers.Instance.spawnedPlayerShip;
            target = spawnedPlayerShip.transform.position - transform.position;
        }

        private void Awake()
        {
            audioSource = gameObject.GetComponent<AudioSource>();
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            SoundManager.Instance.Play(SoundManager.Sound.HitSound);
            if (Hp > 0)
            {
                return;
            }
            SoundManager.Instance.Play(SoundManager.Sound.Explosion);
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
                var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.EnemyBullet);
                if (bullet)
                {
                    bullet.transform.position = gunPosition.position;
                    bullet.transform.rotation = Quaternion.identity;
                    bullet.SetActive(true);
                    bullet.GetComponent<Bullet>().Init(target);    
                }
                fireCounter = 0;
            }
        }
    }
}

