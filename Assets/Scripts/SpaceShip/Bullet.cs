﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceShip;
using UnityEngine;
using Object = System.Object;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D _rigidbody2D;

    public void Init(Vector2 direction)
    {
        Move(direction);
    }

    private void Awake()
    {
        Debug.Assert(_rigidbody2D != null,"rigidbody2D can't be null");
    }

    private void Move(Vector2 direction)
    {
        _rigidbody2D.velocity = direction * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("DestroyGameObject"))
        {
            gameObject.SetActive(false);
            return;
        }

        var target = other.gameObject.GetComponent<IDamageable>();
        this.gameObject.SetActive(false);
        target?.TakeHit(damage);
    }
    
}
