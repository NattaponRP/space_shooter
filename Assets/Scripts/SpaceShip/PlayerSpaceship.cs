﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using SpaceShip;
using UnityEngine;

namespace SpaceShip
{
    public class PlayerSpaceship : BaseSpaceship, IDamageable
    {
        public event Action OnExploded;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet can't be null");
            Debug.Assert(gunPosition != null, "gunPosition can't be null");
            audioSource = gameObject.GetComponent<AudioSource>();
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet)
            {
                bullet.transform.position = gunPosition.position;
                bullet.transform.rotation = Quaternion.identity;
                bullet.SetActive(true);
                bullet.GetComponent<Bullet>().Init(Vector2.up);                
            }   
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            SoundManager.Instance.Play(SoundManager.Sound.HitSound);
            if (Hp > 0)
            {
                return;
            }
            SoundManager.Instance.Play(SoundManager.Sound.Explosion);
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}

